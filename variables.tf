variable "common_vpc_id" {
  description = "The ID of the VPC where the S3 endpoint will be attached."
  type        = string
}

variable "common_vpc_region" {
  description = "The AWS region where the VPC is located."
  type        = string
}

variable "common_bucket_name" {
  description = "The name of the S3 bucket to allow access to."
  type        = string
}

variable "common_allowed_actions" {
  description = "The AWS actions to allow access to the S3 bucket."
  type        = list(string)
  default     = ["s3:ListBucket", "s3:GetObject"]
}

variable "gateway_private_route_table_ids" {
  description = "The IDs of the private Route Table where the S3 gateway endpoint will be attached."
  type        = list(string)
}

variable "interface_endpoint_ip_address_type" {
  description = "The type of IP addresses to associate with the endpoint. This can be either ipv4, dualstack, or ipv6."
  type        = string
  default     = "ipv4"
}

variable "interface_dns_record_ip_type" {
  description = "The type of IP addresses to associate with the DNS record for the endpoint. This can be either ipv4, dualstack, ipv6, or service_defined."
  type        = string
  default     = "ipv4"
}

variable "interface_security_group_ids" {
  description = "The IDs of the Security Group to associate with the endpoint."
  type        = list(string)
}

variable "interface_subnet_ids" {
  description = "The IDs of the Subnet where the S3 endpoint will be attached."
  type        = list(string)
}
