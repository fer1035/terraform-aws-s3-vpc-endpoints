module "s3_endpoints" {
  source = "app.terraform.io/fer1035/s3-vpc-endpoints/aws"

  common_vpc_id                   = "vpc-0a1b2c3d4e5f6g7h8"
  common_vpc_region               = "us-east-1"
  common_bucket_name              = "my-example-bucket"
  gateway_private_route_table_ids = ["rtb-0a1b2c3d4e5f6g7h8"]
  interface_security_group_ids    = ["sg-0a1b2c3d4e5f6g7h8"]
  interface_subnet_ids            = ["subnet-0a1b2c3d4e5f6g7h8"]
}

output "s3_endpoints" {
  value = module.s3_endpoints
}
