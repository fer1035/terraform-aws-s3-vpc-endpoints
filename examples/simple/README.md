# AWS S3 VPC Endpoints Terraform Module

A Terraform module to manage S3 Interface and Gateway endpoints. It creates both Interface and Gateway type endpoints. Gateway endpoint is free, but Interface endpoint charges you for traffic flow.

![Architecture diagram](https://gitlab.com/fer1035/terraform-aws-s3-vpc-endpoints/-/raw/main/images/diagram.png)

## Reference

[AWS Documentation](https://docs.aws.amazon.com/vpc/latest/privatelink/vpc-endpoints-s3.html)

## Policies

- Endpoint policy

  > Defaults to allow everything. This example makes it read-only for all Principals within the corresponding VPC.

  ```json
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "Allow-access-to-specific-bucket",
        "Effect": "Allow",
        "Principal": "*",
        "Action": [
          "s3:ListBucket",
          "s3:GetObject"
        ],
        "Resource": [
          "arn:aws:s3:::bucket_name",
          "arn:aws:s3:::bucket_name/*"
        ]
      }
    ]
  }
  ```

- S3 bucket policy

  > Optional, for when stricter controls for the bucket itself is necessary.

  ```json
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "Allow-access-to-specific-VPCE",
        "Effect": "Deny",
        "Principal": "*",
        "Action": [
          "s3:PutObject",
          "s3:GetObject",
          "s3:DeleteObject"
        ],
        "Resource": ["arn:aws:s3:::bucket_name",
                    "arn:aws:s3:::bucket_name/*"],
        "Condition": {
          "StringNotEquals": {
            "aws:sourceVpce": "vpce-id"
          }
        }
      }
    ]
  }
  ```

- EC2 instance profile policy

  > This example gives the corresponding EC2 read-only access S3.

  ```json
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "s3:ListBucket",
          "s3:GetObject"
        ],
        "Resource": [
          "arn:aws:s3:::bucket_name",
          "arn:aws:s3:::bucket_name/*"
        ]
      }
    ]
  }
  ```

---

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_vpc_endpoint.s3_gateway](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint.s3_interface](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint_policy.gateway_endpoint_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint_policy) | resource |
| [aws_vpc_endpoint_policy.interface_endpoint_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint_policy) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_common_allowed_actions"></a> [common\_allowed\_actions](#input\_common\_allowed\_actions) | The AWS actions to allow access to the S3 bucket. | `list(string)` | <pre>[<br>  "s3:ListBucket",<br>  "s3:GetObject"<br>]</pre> | no |
| <a name="input_common_bucket_name"></a> [common\_bucket\_name](#input\_common\_bucket\_name) | The name of the S3 bucket to allow access to. | `string` | n/a | yes |
| <a name="input_common_vpc_id"></a> [common\_vpc\_id](#input\_common\_vpc\_id) | The ID of the VPC where the S3 endpoint will be attached. | `string` | n/a | yes |
| <a name="input_common_vpc_region"></a> [common\_vpc\_region](#input\_common\_vpc\_region) | The AWS region where the VPC is located. | `string` | n/a | yes |
| <a name="input_gateway_private_route_table_ids"></a> [gateway\_private\_route\_table\_ids](#input\_gateway\_private\_route\_table\_ids) | The IDs of the private Route Table where the S3 gateway endpoint will be attached. | `list(string)` | n/a | yes |
| <a name="input_interface_dns_record_ip_type"></a> [interface\_dns\_record\_ip\_type](#input\_interface\_dns\_record\_ip\_type) | The type of IP addresses to associate with the DNS record for the endpoint. This can be either ipv4, dualstack, ipv6, or service\_defined. | `string` | `"ipv4"` | no |
| <a name="input_interface_endpoint_ip_address_type"></a> [interface\_endpoint\_ip\_address\_type](#input\_interface\_endpoint\_ip\_address\_type) | The type of IP addresses to associate with the endpoint. This can be either ipv4, dualstack, or ipv6. | `string` | `"ipv4"` | no |
| <a name="input_interface_security_group_ids"></a> [interface\_security\_group\_ids](#input\_interface\_security\_group\_ids) | The IDs of the Security Group to associate with the endpoint. | `list(string)` | n/a | yes |
| <a name="input_interface_subnet_ids"></a> [interface\_subnet\_ids](#input\_interface\_subnet\_ids) | The IDs of the Subnet where the S3 endpoint will be attached. | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_s3_endpoints"></a> [s3\_endpoints](#output\_s3\_endpoints) | n/a |
