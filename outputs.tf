output "s3_endpoints" {
  value = {
    interface = aws_vpc_endpoint.s3_interface
    gateway   = aws_vpc_endpoint.s3_gateway
  }
}
