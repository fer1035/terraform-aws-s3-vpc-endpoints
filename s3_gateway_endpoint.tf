resource "aws_vpc_endpoint" "s3_gateway" {
  auto_accept       = true
  route_table_ids   = var.gateway_private_route_table_ids
  service_name      = "com.amazonaws.${var.common_vpc_region}.s3"
  vpc_endpoint_type = "Gateway"
  vpc_id            = var.common_vpc_id
}

resource "aws_vpc_endpoint_policy" "gateway_endpoint_policy" {
  vpc_endpoint_id = aws_vpc_endpoint.s3_gateway.id
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "VPCtoS3",
        "Effect" : "Allow",
        "Principal" : "*",
        "Action" : var.common_allowed_actions,
        "Resource" : [
          "arn:aws:s3:::${var.common_bucket_name}",
          "arn:aws:s3:::${var.common_bucket_name}/*"
        ]
      }
    ]
  })
}
