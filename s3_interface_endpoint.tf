resource "aws_vpc_endpoint" "s3_interface" {
  auto_accept         = true
  ip_address_type     = var.interface_endpoint_ip_address_type
  private_dns_enabled = true
  security_group_ids  = var.interface_security_group_ids
  service_name        = "com.amazonaws.${var.common_vpc_region}.s3"
  subnet_ids          = var.interface_subnet_ids
  vpc_endpoint_type   = "Interface"
  vpc_id              = var.common_vpc_id

  dns_options {
    dns_record_ip_type = var.interface_dns_record_ip_type
    # private_dns_only_for_inbound_resolver_endpoint = true
  }

  depends_on = [
    aws_vpc_endpoint.s3_gateway
  ]
}

resource "aws_vpc_endpoint_policy" "interface_endpoint_policy" {
  vpc_endpoint_id = aws_vpc_endpoint.s3_interface.id
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "VPCtoS3",
        "Effect" : "Allow",
        "Principal" : "*",
        "Action" : var.common_allowed_actions,
        "Resource" : [
          "arn:aws:s3:::${var.common_bucket_name}",
          "arn:aws:s3:::${var.common_bucket_name}/*"
        ]
      }
    ]
  })
}
